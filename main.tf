data "aws_instances" "test" {
  
}

data "aws_instances" "test1" {
  provider = aws.dev
}
output "prod" {
  value = tolist(data.aws_instances.test.private_ips)
}
output "dev" {

  value = tolist(data.aws_instances.test1.private_ips)
}


resource "local_file" "hosts_cfg" {
  content = templatefile("inventory",
    {
      kafka_processors = data.aws_instances.test.private_ips
      test_clients = data.aws_instances.test1.private_ips
    }
  )
  filename = "hosts.cfg"
}


variable "access_key"{
 

}

variable "secret_key" {
   

}