terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.55.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  access_key = var.access_key
  secret_key = var.secret_key
}

provider "aws" {
  region = "ap-south-1"
  alias = "dev"
  access_key = var.access_key
  secret_key = var.secret_key

}
